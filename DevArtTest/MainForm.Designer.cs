﻿namespace DevArtTest.UI
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fileTextPanel = new System.Windows.Forms.RichTextBox();
            this.menuBar = new System.Windows.Forms.MenuStrip();
            this.fileMenuButton = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFileMenuButton = new System.Windows.Forms.ToolStripMenuItem();
            this.loadFileMenuButton = new System.Windows.Forms.ToolStripMenuItem();
            this.newFileMenuButton = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // fileTextPanel
            // 
            this.fileTextPanel.AcceptsTab = true;
            this.fileTextPanel.BulletIndent = 1;
            this.fileTextPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.fileTextPanel.Location = new System.Drawing.Point(12, 31);
            this.fileTextPanel.Name = "fileTextPanel";
            this.fileTextPanel.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.fileTextPanel.Size = new System.Drawing.Size(1160, 638);
            this.fileTextPanel.TabIndex = 0;
            this.fileTextPanel.Text = "";
            // 
            // menuBar
            // 
            this.menuBar.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.menuBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenuButton});
            this.menuBar.Location = new System.Drawing.Point(0, 0);
            this.menuBar.Name = "menuBar";
            this.menuBar.Size = new System.Drawing.Size(1184, 27);
            this.menuBar.TabIndex = 1;
            this.menuBar.Text = "fileMenu";
            // 
            // fileMenuButton
            // 
            this.fileMenuButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newFileMenuButton,
            this.saveFileMenuButton,
            this.loadFileMenuButton});
            this.fileMenuButton.Name = "fileMenuButton";
            this.fileMenuButton.Size = new System.Drawing.Size(37, 20);
            this.fileMenuButton.Text = "File";
            // 
            // saveFileMenuButton
            // 
            this.saveFileMenuButton.Name = "saveFileMenuButton";
            this.saveFileMenuButton.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveFileMenuButton.Size = new System.Drawing.Size(180, 22);
            this.saveFileMenuButton.Text = "Save";
            this.saveFileMenuButton.Click += new System.EventHandler(this.saveFileMenuButton_Click);
            // 
            // loadFileMenuButton
            // 
            this.loadFileMenuButton.Name = "loadFileMenuButton";
            this.loadFileMenuButton.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
            this.loadFileMenuButton.Size = new System.Drawing.Size(180, 22);
            this.loadFileMenuButton.Text = "Load";
            this.loadFileMenuButton.Click += new System.EventHandler(this.loadFileMenuButton_Click);
            // 
            // newFileMenuButton
            // 
            this.newFileMenuButton.Name = "newFileMenuButton";
            this.newFileMenuButton.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.newFileMenuButton.Size = new System.Drawing.Size(180, 22);
            this.newFileMenuButton.Text = "New";
            this.newFileMenuButton.Click += new System.EventHandler(this.newFileMenuButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1184, 681);
            this.Controls.Add(this.fileTextPanel);
            this.Controls.Add(this.menuBar);
            this.MainMenuStrip = this.menuBar;
            this.Name = "MainForm";
            this.Text = "Undefined";
            this.menuBar.ResumeLayout(false);
            this.menuBar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox fileTextPanel;
        private System.Windows.Forms.MenuStrip menuBar;
        private System.Windows.Forms.ToolStripMenuItem fileMenuButton;
        private System.Windows.Forms.ToolStripMenuItem saveFileMenuButton;
        private System.Windows.Forms.ToolStripMenuItem loadFileMenuButton;
        private System.Windows.Forms.ToolStripMenuItem newFileMenuButton;
    }
}

