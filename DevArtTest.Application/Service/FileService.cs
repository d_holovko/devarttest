﻿namespace DevArtTest.Application.Service
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using DevArtTest.Application.Service.Abstract;
    using DevArtTest.Persistence.Repository;
    using DevArtTest.Persistence.Repository.Abstract;

    using DevArtTest.Persistence.Model;

    public class FileService : IFileService<int, File>
    {
        private readonly IFileRepository<int, File> repository;

        public FileService()
        {
            repository = new FileRepository();
        }

        public async Task SaveAsync(File file)
        {
            await file.CompressAsync();

            if (file.Id == 0)
            {
                file.Created = file.Edited = DateTime.Now;

                await repository.CreateAsync(file);

                return;
            }

            file.Edited = DateTime.Now;

            await repository.UpdateAsync(file);
        }

        public async Task<File> LoadAsync(int id)
        {
            var file = await repository.GetAsync(id);

            await file.DecompressAsync();

            return file;
        }

        public async Task<IEnumerable<File>> LoadAsync()
        {
            var files = await repository.GetAsync();

            return files;
        }

        public async Task<IEnumerable<File>> LoadInfoAsync()
        {
            var fileInfos = await repository.GetAsync(x => new File
            {
                Id = x.Id,
                Title = x.Title,
                Data = null,
                Created = x.Created,
                Edited = x.Edited

            });

            return fileInfos;
        }
    }
}
