﻿namespace DevArtTest.Persistence.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Threading.Tasks;

    using DevArtTest.Persistence.Model;
    using DevArtTest.Persistence.Repository.Abstract;

    public class FileRepository : IFileRepository<int, File>
    {
        public async Task CreateAsync(File file)
        {
            using (var context = new FilesContext())
            {
                context.Files.Add(file);

                await context.SaveChangesAsync();
            }
        }

        public async Task<IEnumerable<File>> GetAsync()
        {
            using (var context = new FilesContext())
            {
                var files = await context.Files.ToArrayAsync();

                return files;
            }
        }

        public async Task<IEnumerable<File>> GetAsync(Func<File, File> func)
        {
            using (var context = new FilesContext())
            {
                return await Task.FromResult(context.Files.Select(func).ToArray());
            }
        }

        public async Task<File> GetAsync(int id)
        {
            using (var context = new FilesContext())
            {
                var file = await context.Files.FindAsync(id);

                return file;
            }
        }

        public async Task UpdateAsync(File file)
        {
            using (var context = new FilesContext())
            {
                context.Entry(file).State = EntityState.Modified;

                await context.SaveChangesAsync();
            }
        }

        public async Task DeleteAsync(int id)
        {
            using (var context = new FilesContext())
            {
                var file = await GetAsync(id);

                context.Entry(file).State = EntityState.Deleted;

                await context.SaveChangesAsync();
            }
        }
    }
}
