﻿namespace DevArtTest.Application.Service.Abstract
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IFileService<TKey, TEntity>
    {
        Task SaveAsync(TEntity file);

        Task<TEntity> LoadAsync(TKey id);

        Task<IEnumerable<TEntity>> LoadAsync();

        Task<IEnumerable<TEntity>> LoadInfoAsync();
    }
}
