﻿namespace DevArtTest.UI
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;

    using DevArtTest.Application.Service;
    using DevArtTest.Application.Service.Abstract;
    using DevArtTest.Persistence.Model;

    public partial class LoadFileForm : Form
    {
        private const string DATE_TIME_FORMAT = "g";

        private readonly IFileService<int, File> fileService;

        private IEnumerable<File> files;

        public int SelectedFileId { get; set; }

        public LoadFileForm()
        {
            InitializeComponent();

            fileService = new FileService();
        }

        protected override async void OnLoad(EventArgs e)
        {
            files = await fileService.LoadInfoAsync();

            foreach (var file in files)
            {
                var item = new ListViewItem(
                    new[] 
                        {
                            file.Title,
                            file.Created.ToString(DATE_TIME_FORMAT),
                            file.Edited.ToString(DATE_TIME_FORMAT)

                        });

                item.Tag = file.Id;

                filesList.Items.Add(item);
            }

            base.OnLoad(e);
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            this.SelectedFileId = (int)filesList.SelectedItems[0].Tag;
        }
    }
}
