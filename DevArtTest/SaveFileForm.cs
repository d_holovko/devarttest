﻿namespace DevArtTest.UI
{
    using System;
    using System.Windows.Forms;

    public partial class SaveFileForm : Form
    {
        public string FileName { get; set; }

        public SaveFileForm()
        {
            InitializeComponent();
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            this.FileName = fileNameInput.Text;
        }
    }
}
