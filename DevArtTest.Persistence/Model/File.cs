﻿namespace DevArtTest.Persistence.Model
{
    using System;

    public class File
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public byte[] Data { get; set; }

        public DateTime Created { get; set; }

        public DateTime Edited { get; set; }
    }
}
