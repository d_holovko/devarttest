﻿namespace DevArtTest.Persistence
{
    using System.Data.Entity;

    using DevArtTest.Persistence.Model;

    public class FilesContext : DbContext
    {
        public FilesContext() : base("textFilesDb")
        {
            Configuration.AutoDetectChangesEnabled = false;
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }

        public DbSet<File> Files { get; set; }
    }
}
