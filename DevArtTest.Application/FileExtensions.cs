﻿namespace DevArtTest.Application
{
    using File = DevArtTest.Persistence.Model.File;
    using System.Threading.Tasks;
    using System.IO;
    using ICSharpCode.SharpZipLib.GZip;

    public static class FileExtensions
    {
        public static async Task CompressAsync(this File file)
        {
            using (var result = new MemoryStream())
            {
                using (var compressor = new GZipOutputStream(result))
                {
                    await compressor.WriteAsync(file.Data, 0, file.Data.Length);
                }

                file.Data = result.ToArray();
            }
        }

        public static async Task DecompressAsync(this File file)
        {
            var result = new MemoryStream();
            var buffer = new byte[8];

            using (var decompressor = new GZipInputStream(new MemoryStream(file.Data)))
            {
                int count = -1;

                while ((count = await decompressor.ReadAsync(buffer, 0, buffer.Length)) != 0)
                {
                    await result.WriteAsync(buffer, 0, count);
                }
            }

            file.Data = result.ToArray();
        }
    }
}
