﻿namespace DevArtTest.Persistence.Repository.Abstract
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IFileRepository<TKey, TEntity>
    {
        Task CreateAsync(TEntity file);

        Task<IEnumerable<TEntity>> GetAsync();

        Task<IEnumerable<TEntity>> GetAsync(Func<TEntity, TEntity> func);

        Task<TEntity> GetAsync(TKey id);

        Task UpdateAsync(TEntity file);

        Task DeleteAsync(TKey id);
    }
}
