﻿namespace DevArtTest.UI
{
    using System;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    using DevArtTest.Application.Service;
    using DevArtTest.Application.Service.Abstract;
    using DevArtTest.Persistence.Model;

    public partial class MainForm : Form
    {
        private readonly IFileService<int, File> fileService;

        private File file;

        public MainForm()
        {
            InitializeComponent();

            fileService = new FileService();

            file = new File();
        }

        private async void saveFileMenuButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (file.Id == 0)
                {
                    await SaveNewAsync();

                    return;
                }

                await SaveExistingAsync();
            }
            catch (Exception exception)
            {
                MessageBox.Show(
                    "Unfortunately save failed",
                    "Save failed",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private async void loadFileMenuButton_Click(object sender, EventArgs e)
        {
            try
            {
                await LoadFileAsync();
            }
            catch (Exception exception)
            {
                MessageBox.Show(
                    "Unfortunately load failed",
                    "Save failed",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private async void newFileMenuButton_Click(object sender, EventArgs e)
        {
            try
            {
                await CreateFileAsync();
            }
            catch (Exception exception)
            {
                MessageBox.Show(
                    "Unfortunately creation failed",
                    "Save failed",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private async Task SaveNewAsync()
        {
            using (var form = new SaveFileForm())
            {
                if (form.ShowDialog(this) != DialogResult.OK)
                {
                    return;
                }

                this.file.Title = form.FileName;

                this.file.Data = Encoding.UTF8.GetBytes(fileTextPanel.Rtf);

                await fileService.SaveAsync(file);

                this.Text = file.Title;
            }
        }

        private async Task SaveExistingAsync()
        {
            this.file.Data = Encoding.UTF8.GetBytes(fileTextPanel.Rtf);

            await fileService.SaveAsync(file);
        }

        private async Task LoadFileAsync()
        {
            using (var form = new LoadFileForm())
            {
                if (form.ShowDialog(this) != DialogResult.OK)
                {
                    return;
                }

                this.file = await fileService.LoadAsync(form.SelectedFileId);

                fileTextPanel.Rtf = Encoding.UTF8.GetString(file.Data);

                this.Text = file.Title;
            }
        }

        private async Task CreateFileAsync()
        {
            if (MessageBox.Show(
                    "All unsaved data will be discarded", 
                    "New file", 
                    MessageBoxButtons.OKCancel, 
                    MessageBoxIcon.Warning) != DialogResult.OK)
            {
                return;
            }

            fileTextPanel.Clear();

            this.file = new File();

            this.Text = "Undefined";
        }
    }
}
